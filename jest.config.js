module.exports = {
  "collectCoverageFrom": ["src/**/*.js", "!**/node_modules/**"],
  // "coverageProvider": 'v8',
  "coverageReporters": ["html", "text", "text-summary", "cobertura"],
  "testMatch": ["**/*.test.js"]
}

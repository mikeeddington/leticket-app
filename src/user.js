"use strict";

const _sequelize = require('sequelize');
const express = require('express');
const router = express.Router();
// const sequelize = new _sequelize('postgres://leticket_user:leticket_password@localhost:5432/leticket-db') // Example for postgres
// const sequelize = new _sequelize(process.env.POSTGRES_DB, process.env.POSTGRES_USER, process.env.POSTGRES_PASSWORD, {
//     host: process.env.POSTGRES_HOST,
//     dialect: 'postgres',
// })

module.exports = (sequelize) => {
    const User = sequelize.define('User', {
        id: {
          type: _sequelize.UUID,
          defaultValue: _sequelize.UUIDV4,
          allowNull: false,
          primaryKey: true
        },
        username: _sequelize.DataTypes.STRING,
      });
      
      User.sync({force: true});
      
      router.get('/', async (req, res, next) => {
          const users = await User.findAll();
          res.send(users);
      });
      
      router.post('/', async (req, res, next) => {
          const jane = await User.create({
              username: req.body.name,
          });
          await jane.save();
          res.send(jane);
      });
      
      router.get('/:id', async (req, res, next) => {
          const user = await User.findByPk(req.params.id);
          res.send(user);
      });
      
      router.post('/:id', async (req, res, next) => {
          const user = await User.findByPk(req.params.id);
          user.name = req.body.name;
          res.send(user);
      });

    return router;
};
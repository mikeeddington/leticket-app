require('dotenv').config()
const express = require('express'); 
const cors = require('cors');
const app = express()

const { Sequelize } = require('sequelize');
const { log } = require('console');

const sequelize = new Sequelize(process.env.POSTGRES_DB, process.env.POSTGRES_USER, process.env.POSTGRES_PASSWORD, {
    host: process.env.POSTGRES_HOST,
    dialect: 'postgres',
});

app.set('port', process.env.PORT || 8080);

app.use(express.static('public'));
app.use(express.static('dist'));

app.use(express.json());

app.use(cors());

app.use('/users', require('./user')(sequelize));

const server = app.listen(app.get('port'), async err => {
    if (err) throw err;
    await sequelize.authenticate();
    console.log('Connection has been established successfully.');
    console.log(`app listening on port: ${app.get('port')}`);
});

module.exports = {
    init: async () => {
        await sequelize.authenticate();
        await sequelize.sync({ force: true }); // this recreates all table at loading time
        console.log('table were recreated');
        server.closeAll = async () => {
            await sequelize.close();
            return server.close();
        }
        return server;
    },

    close: async () => {
        await sequelize.close();
        return server.close();
    }
};
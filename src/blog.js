"use strict";

var _sequelize = require("sequelize");
const express = require('express');
const router = express.Router();
// const sequelize = new _sequelize('postgres://leticket_user:leticket_password@localhost:5432/leticket-db') // Example for postgres

// write an entity of blogs using sequelize and create associated routes


module.exports = (sequelize) => {


  const Blog = sequelize.define('Blog', {
    id: {
      type: _sequelize.UUID,
      defaultValue: _sequelize.UUIDV4,
      allowNull: false,
      primaryKey: true
    },
    content: _sequelize.DataTypes.STRING,
  });

  // create a route for the entity to get all blogs
  router.get('/', async (req, res) => {
    const blogs = await Blog.findAll();
    res.json(blogs);
  });

  router.get('/blogs/:id', (req, res) => {
    Blog.findByPk(req.params.id).then(blog => {
      res.send(blog);
    });
  });

  // create a post route for my blog entity
  router.post('/blogs', (req, res) => {
    Blog.create({
      content: req.body.content
    }).then(blog => {
      res.send(blog);
    });
  });

  // create a modification route to update a blog post
  router.put('/blogs/:id', (req, res) => {
    Blog.update({
      content: req.body.content
    }, { where: { id: req.params.id } }).then(blog => {
      res.send(blog);
    });
  });

  return router;
};


